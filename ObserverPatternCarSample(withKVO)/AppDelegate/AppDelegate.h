//
//  AppDelegate.h
//  ObserverPatternCarSample(withKVO)
//
//  Created by Vladyslav Bedro on 10/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow* window;


@end

