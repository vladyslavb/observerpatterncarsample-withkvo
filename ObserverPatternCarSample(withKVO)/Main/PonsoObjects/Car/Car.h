//
//  Car.h
//  ObserverPatternCarSample(withKVO)
//
//  Created by Vladyslav Bedro on 10/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSObject

//property
@property (strong, nonatomic) NSString*       model;
@property (strong, nonatomic) NSString*       engine;
@property (strong, nonatomic) NSString*       autoPilotVersion;
@property (assign, nonatomic) NSUInteger      wheelsSize;
@property (assign, nonatomic) NSUInteger      speed;
@property (assign, nonatomic) NSUInteger      gasoline;
@property (strong, nonatomic) NSMutableArray* passengers;

@end

NS_ASSUME_NONNULL_END
