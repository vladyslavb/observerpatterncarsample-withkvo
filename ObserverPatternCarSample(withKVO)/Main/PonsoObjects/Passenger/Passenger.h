//
//  Passenger.h
//  ObserverPatternCarSample(withKVO)
//
//  Created by Vladyslav Bedro on 10/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Passenger : NSObject

//property
@property (strong, nonatomic) NSString*  firstName;
@property (strong, nonatomic) NSString*  lastName;
@property (assign, nonatomic) NSUInteger age;
@property (assign, nonatomic) BOOL       isInTheCarNow;

@end

NS_ASSUME_NONNULL_END
