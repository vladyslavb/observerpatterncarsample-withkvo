//
//  Passenger.m
//  ObserverPatternCarSample(withKVO)
//
//  Created by Vladyslav Bedro on 10/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Passenger.h"

//classes
#import "Car.h"

@implementation Passenger


#pragma mark - KVO methods -

- (void) observeValueForKeyPath: (NSString*)                             keyPath
                       ofObject: (id)                                    object
                         change: (NSDictionary<NSKeyValueChangeKey,id>*) change
                        context: (void*)                                 context
{
    if([keyPath isEqualToString: @"gasoline"])
    {
        if(((Car*) object).gasoline <= 20)
        {
            NSLog(@"Бензин закончился, пассажиру прийдется покинуть машину..");
            
            self.isInTheCarNow = NO;
        }
            
    }
}

@end
