//
//  MainViewController.m
//  ObserverPatternCarSample(withKVO)
//
//  Created by Vladyslav Bedro on 10/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

//classes
#import "Car.h"
#import "Passenger.h"

@interface MainViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UILabel* messageAreaLabel;

//properties
@property (strong, nonatomic) Car*            car;
@property (strong, nonatomic) NSMutableArray* passengersArray;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNotifications];
}

- (void) dealloc
{
    for(Passenger* passenger in self.passengersArray)
    {
        [self.car removeObserver: passenger
                      forKeyPath: @"gasoline"];
    }
}


#pragma mark - Internal methods -

- (void) configureNotifications
{
    self.car = [[Car alloc] init];
    
    self.passengersArray = [NSMutableArray new];
    
    Passenger* frontSideFirstPassenger  = [[Passenger alloc] init];
    Passenger* frontSideSecondPassenger = [[Passenger alloc] init];
    Passenger* backSideFirstPassenger   = [[Passenger alloc] init];
    Passenger* backSideSecondPassenger  = [[Passenger alloc] init];
    
    [self.passengersArray addObject: frontSideFirstPassenger];
    [self.passengersArray addObject: frontSideSecondPassenger];
    [self.passengersArray addObject: backSideFirstPassenger];
    [self.passengersArray addObject: backSideSecondPassenger];
    
    self.car.passengers = self.passengersArray;
    
    frontSideFirstPassenger.isInTheCarNow  = YES;
    frontSideSecondPassenger.isInTheCarNow = YES;
    backSideFirstPassenger.isInTheCarNow   = YES;
    backSideSecondPassenger.isInTheCarNow  = YES;
    
    self.car.gasoline = 80;
    
    self.messageAreaLabel.text = [NSString stringWithFormat: @"Car gasoline balance: %ld", self.car.gasoline];
    
    for(Passenger* passenger in self.passengersArray)
    {
        [self.car addObserver: passenger
                   forKeyPath: @"gasoline"
                      options: NSKeyValueObservingOptionNew
                      context: nil];
    }
}


#pragma mark - Actions -

- (IBAction) onCarGoButtonPressed: (UIButton*) sender
{
    if(self.car.gasoline > 20)
    {
        [self.car setValue: @(self.car.gasoline - 20)
                    forKey: @"gasoline"];
        
        self.messageAreaLabel.text = [NSString stringWithFormat: @"Car gasoline balance: %ld", self.car.gasoline];
    }
    else
    {
        NSUInteger outCarPassengerCounter = 0;
        
        for(Passenger* passenger in self.passengersArray)
        {
            if(passenger.isInTheCarNow == NO)
            {
                outCarPassengerCounter++;
            }
        }
        
        self.messageAreaLabel.text = [NSString stringWithFormat: @"Ran out of gas! The number of passengers who got out of the car: %ld", outCarPassengerCounter];
    }
}

@end
